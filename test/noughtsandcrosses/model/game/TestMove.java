package noughtsandcrosses.model.game;

import static org.junit.jupiter.api.Assertions.*;

import noughtsandcrosses.model.GamePoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for the Move object that represents a move made by a human or player
 */
public class TestMove {

    private Move sut;

    @BeforeEach
    public void setUp() {
        sut = new Move("Player One", new GamePoint(0,0));
    }

    @Test
    public void testConstructor() {
        assertEquals("Player One", sut.getPlayer());
        assertEquals(new GamePoint(0, 0), sut.getLocation());
    }

    @Test
    public void testToString() {
        assertEquals("Player One claimed left column, top row", sut.toString());
    }

    @Test
    public void testEqualsIdentifiesEqualMoves() {
        Move otherMove = new Move("Player One", new GamePoint(0, 0));

        assertEquals(otherMove, sut);
    }

    @Test
    public void testEqualsDoesNotMatchDifferentMoves() {
        Move otherMove = new Move("Player Two", new GamePoint(0, 0));
        Move thirdMove = new Move("Player One", new GamePoint(0, 1));
        Move fourthMove = new Move("Player Two", new GamePoint(1, 1));
        Move fifthMove = new Move("Player One", new GamePoint(1, 1));

        assertNotEquals(otherMove, sut);
        assertNotEquals(thirdMove, sut);
        assertNotEquals(fourthMove, sut);
        assertNotEquals(fifthMove, sut);
    }
}
