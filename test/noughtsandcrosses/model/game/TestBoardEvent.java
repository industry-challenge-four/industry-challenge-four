package noughtsandcrosses.model.game;

import static org.junit.jupiter.api.Assertions.*;

import noughtsandcrosses.model.CellState;
import noughtsandcrosses.model.GamePoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class TestBoardEvent {

    private BoardEvent sut;

    @BeforeEach
    public void setUp() {
        Move lastMove = new Move("Player One", new GamePoint(0, 0));
        Map<GamePoint, CellState> state = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                state.putIfAbsent(new GamePoint(i, j), CellState.EMPTY);
            }
        }
        String message = "Test Message";

        sut = new BoardEvent(state, lastMove, message);
    }

    @Test
    public void testConstructorLastMoveAndMethod() {
        Map<GamePoint, CellState> state = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                state.putIfAbsent(new GamePoint(i, j), CellState.EMPTY);
            }
        }
        Move lastMove = new Move("Player One", new GamePoint(0, 0));


        assertEquals(state, sut.getState());
        assertEquals(lastMove, sut.getLastMove());
        assertEquals("Test Message", sut.getMessage());
    }

    @Test
    public void testConstructorMessageOnly() {
        Map<GamePoint, CellState> state = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                state.putIfAbsent(new GamePoint(i, j), CellState.EMPTY);
            }
        }
        String message = "Test Message";

        sut = new BoardEvent(state, message);

        assertNull(sut.getLastMove());
        assertEquals(state, sut.getState());
        assertEquals("Test Message", sut.getMessage());
    }

    @Test
    public void testConstructorLastMoveOnly() {
        Move lastMove = new Move("Player One", new GamePoint(0, 0));
        Map<GamePoint, CellState> state = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                state.putIfAbsent(new GamePoint(i, j), CellState.EMPTY);
            }
        }

        sut = new BoardEvent(state, lastMove);

        assertEquals(lastMove, sut.getLastMove());
        assertEquals(state, sut.getState());
        assertNull(sut.getMessage());
    }

}
