package noughtsandcrosses.model.game;

import noughtsandcrosses.model.GamePoint;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestBoardStateAnalyser {

    @Test
    public void testBoardStateAnalyserEmptyBoardSuggestsTopLeft() {
        BoardStateAnalyser sut = new BoardStateAnalyser("012345678");

        assertEquals(new GamePoint(0, 0), sut.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(0, 0), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserTopLeftClaimedSuggestsCentre() {
        BoardStateAnalyser sut = new BoardStateAnalyser("X12345678");

        assertEquals(new GamePoint(1, 1), sut.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(1, 1), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserSuggestsTopLeftCornerOnSecondMove() {
        BoardStateAnalyser sut = new BoardStateAnalyser("0123X5678");

        assertEquals(new GamePoint(0, 0), sut.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(0, 0), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserSuggestsWinningPositionOne() {
        BoardStateAnalyser state1 = new BoardStateAnalyser("X1XO4O678");

        assertEquals(new GamePoint(1, 0), state1.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(1, 1), state1.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserSuggestsWinningPositionTwo() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XX23456OO");

        assertEquals(new GamePoint(2, 0), sut.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(0, 2), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserSuggestsWinningPositionThree() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XOX3X567O");

        assertEquals(new GamePoint(0, 2), sut.getOptimalPosition("X", "O"));

        // verify O blocks the winning position
        assertEquals(new GamePoint(0, 2), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserSuggestsWinningPositionFour() {
        BoardStateAnalyser sut = new BoardStateAnalyser("X12OX5X7O");

        assertEquals(new GamePoint(2, 0), sut.getOptimalPosition("X", "O"));

        // verify O blocks the winning position
        assertEquals(new GamePoint(2, 0), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserSuggestsWinningPositionFive() {
        BoardStateAnalyser sut = new BoardStateAnalyser("012XX5O7O");

        assertEquals(new GamePoint(2, 1), sut.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(1, 2), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserSuggestsWinningPositionSix() {
        BoardStateAnalyser sut = new BoardStateAnalyser("01XOOX678");

        assertEquals(new GamePoint(2, 2), sut.getOptimalPosition("X", "O"));

        // verify O blocks the winning position
        assertEquals(new GamePoint(2, 2), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserBlocksOpponentWinningPositionIfNoWinningStateOne() {
        BoardStateAnalyser sut = new BoardStateAnalyser("01XOOX678");

        assertEquals(new GamePoint(2, 2), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserBlocksOpponentWinningPositionIfNoWinningStateTwo() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XOX3X567O");

        assertEquals(new GamePoint(0, 2), sut.getOptimalPosition("O", "X"));

        // verifying winning position returned for X
        assertEquals(new GamePoint(0, 2), sut.getOptimalPosition("X", "O"));
    }

    @Test
    public void testBoardStateAnalyserBlocksOpponentWinningPositionIfNoWinningStateThree() {
        BoardStateAnalyser sut = new BoardStateAnalyser("X12OX5X7O");

        assertEquals(new GamePoint(2, 0), sut.getOptimalPosition("O", "X"));

        // verifying winning position returned for X
        assertEquals(new GamePoint(2, 0), sut.getOptimalPosition("X", "O"));
    }

    @Test
    public void testBoardStateAnalyserSetsUpWinIfNoWinningMovesOne() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XO23X567O");

        assertEquals(new GamePoint(0,2), sut.getOptimalPosition("X", "O"));

        // verifying O blocks the move as no better options exist
        assertEquals(new GamePoint(0,2), sut.getOptimalPosition("O", "X"));
    }


    @Test
    public void testBoardStateAnalyserSetsUpWinIfNoWinningMovesTwo() {
        BoardStateAnalyser sut = new BoardStateAnalyser("X12OX567O");

        assertEquals(new GamePoint(2,0), sut.getOptimalPosition("X", "O"));

        // verifying O blocks the move as no better options exist
        assertEquals(new GamePoint(2,0), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserSetsUpWinIfNoWinningMovesThree() {
        BoardStateAnalyser sut = new BoardStateAnalyser("1O2XXO678");

        assertEquals(new GamePoint(0,2), sut.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(2,0), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserSetsUpWinIfNoWinningMovesFour() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XXO34X67O");

        assertEquals(new GamePoint(1,1), sut.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(0,2), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserBlocksOpponentWinSetupIfCantSetupOwnOne() {
        BoardStateAnalyser sut = new BoardStateAnalyser("X123O567X");

        assertEquals(new GamePoint(0, 2), sut.getOptimalPosition("X", "O"));

        // O should block the win
        assertEquals(new GamePoint(1, 2), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserBlocksOpponentWinSetupIfCantSetupOwnTwo() {
        BoardStateAnalyser sut = new BoardStateAnalyser("01X3O5X78");

        assertEquals(new GamePoint(1, 2), sut.getOptimalPosition("O", "X"));

        // X should try to set up the win
        assertEquals(new GamePoint(2, 2), sut.getOptimalPosition("X", "O"));
    }

    @Test
    public void testBoardStateAnalyserReturnsMoveEvenWhenNoSetupsOne() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XOX3O5OXX");

        assertEquals(new GamePoint(2, 1), sut.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(2, 1), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserReturnsMoveEvenWhenNoSetupsTwo() {
        BoardStateAnalyser sut = new BoardStateAnalyser("X1OOOXX7X");

        assertEquals(new GamePoint(1, 2), sut.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(1, 2), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testBoardStateAnalyserReturnsMoveEvenWhenNoSetupsThree() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XXOOOXX7O");

        assertEquals(new GamePoint(1, 2), sut.getOptimalPosition("X", "O"));
        assertEquals(new GamePoint(1, 2), sut.getOptimalPosition("O", "X"));
    }

    @Test
    public void testIsWinnerReturnsWinnersOneX() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XXXO4O678");
        assertTrue(sut.isWinner("X"));
        assertFalse(sut.isWinner("O"));
    }

    @Test
    public void testIsWinnerReturnsWinnersTwoX() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XO2XO5X78");
        assertTrue(sut.isWinner("X"));
        assertFalse(sut.isWinner("O"));
    }

    @Test
    public void testIsWinnerReturnsWinnersThreeX() {
        BoardStateAnalyser sut = new BoardStateAnalyser("OO1XXX678");
        assertTrue(sut.isWinner("X"));
        assertFalse(sut.isWinner("O"));
    }

    @Test
    public void testIsWinnerReturnsWinnersFourX() {
        BoardStateAnalyser sut = new BoardStateAnalyser("OO2345XXX");
        assertTrue(sut.isWinner("X"));
        assertFalse(sut.isWinner("O"));
    }

    @Test
    public void testIsWinnerReturnsWinnersFiveX() {
        BoardStateAnalyser sut = new BoardStateAnalyser("OXO3X56X8");
        assertTrue(sut.isWinner("X"));
        assertFalse(sut.isWinner("O"));
    }

    @Test
    public void testIsWinnerReturnsWinnersSixX() {
        BoardStateAnalyser sut = new BoardStateAnalyser("OOX34X67X");
        assertTrue(sut.isWinner("X"));
        assertFalse(sut.isWinner("O"));
    }

    @Test
    public void testIsWinnerReturnsWinnersSevenX() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XOO3X567X");
        assertTrue(sut.isWinner("X"));
        assertFalse(sut.isWinner("O"));
    }

    @Test
    public void testIsWinnerReturnsWinnersEightX() {
        BoardStateAnalyser sut = new BoardStateAnalyser("OOX3X5X78");
        assertTrue(sut.isWinner("X"));
        assertFalse(sut.isWinner("O"));
    }

    @Test
    public void testIsWinnerReturnsWinnersOneO() {
        BoardStateAnalyser sut = new BoardStateAnalyser("OXXO45O78");
        assertTrue(sut.isWinner("O"));
        assertFalse(sut.isWinner("X"));
    }

    @Test
    public void testIsWinnerReturnsWinnersTwoO() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XXO3O5O78");
        assertTrue(sut.isWinner("O"));
        assertFalse(sut.isWinner("X"));
    }

    @Test
    public void testIsWinnerReturnsWinnersThreeO() {
        BoardStateAnalyser sut = new BoardStateAnalyser("OOOX4X678");
        assertTrue(sut.isWinner("O"));
        assertFalse(sut.isWinner("X"));
    }

    @Test
    public void testIsWinnerReturnsFalseIfNoWinnerOne() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XXOOOXXOX");
        assertFalse(sut.isWinner("O"));
        assertFalse(sut.isWinner("X"));
    }

    @Test
    public void testIsWinnerReturnsFalseIfNoWinnerTwo() {
        BoardStateAnalyser sut = new BoardStateAnalyser("OXOXXOXOX");
        assertFalse(sut.isWinner("O"));
        assertFalse(sut.isWinner("X"));
    }

    @Test
    public void testIsWinnerReturnsFalseIfNoWinnerThree() {
        BoardStateAnalyser sut = new BoardStateAnalyser("XOXXOOOXX");
        assertFalse(sut.isWinner("O"));
        assertFalse(sut.isWinner("X"));
    }
}
