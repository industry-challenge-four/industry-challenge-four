package noughtsandcrosses.model.game;

import org.junit.jupiter.api.Test;

/**
 * Abstract test class that should be extended by any class providing unit tests for an object implementing the ModelObserver interface
 */
public abstract class TestModelObserver {

    @Test
    public abstract void testModelStateChange();

}
