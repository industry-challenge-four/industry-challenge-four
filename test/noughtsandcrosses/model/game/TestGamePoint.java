package noughtsandcrosses.model.game;

import static org.junit.jupiter.api.Assertions.*;

import noughtsandcrosses.model.GamePoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestGamePoint {

    private GamePoint sut;

    @BeforeEach
    public void setUp() {
        sut = new GamePoint(1, 2);
    }

    @Test
    public void testConstructor() {
        assertEquals(1, sut.x);
        assertEquals(2, sut.y);
    }

    @Test
    public void testToStringTopLeftCorner() {
        GamePoint sut = new GamePoint(0, 0);

        assertEquals("left column, top row", sut.toString());
    }

    @Test
    public void testToStringTopCentre() {
        GamePoint sut = new GamePoint(0, 1);

        assertEquals("left column, centre row", sut.toString());
    }

    @Test
    public void testToStringTopRightCorner() {
        GamePoint sut = new GamePoint(0, 2);

        assertEquals("left column, bottom row", sut.toString());
    }

    @Test
    public void testToStringLeftCentre() {
        GamePoint sut = new GamePoint(1, 0);

        assertEquals("centre column, top row", sut.toString());
    }

    @Test
    public void testToStringCentre() {
        GamePoint sut = new GamePoint(1, 1);

        assertEquals("centre column, centre row", sut.toString());
    }

    @Test
    public void testToStringRightCentre() {
        GamePoint sut = new GamePoint(1, 2);

        assertEquals("centre column, bottom row", sut.toString());
    }

    @Test
    public void testToStringBottomLeftCorner() {
        GamePoint sut = new GamePoint(2, 0);

        assertEquals("right column, top row", sut.toString());
    }

    @Test
    public void testToStringBottomCentre() {
        GamePoint sut = new GamePoint(2, 1);

        assertEquals("right column, centre row", sut.toString());
    }

    @Test
    public void testToStringBottomRightCorner() {
        GamePoint sut = new GamePoint(2, 2);

        assertEquals("right column, bottom row", sut.toString());
    }

    @Test
    public void testInvalidXCoordinate() {
        try {
            new GamePoint(3, 0);
            fail();
        } catch (InvalidLocationException e) {
            assertEquals("(3, 0) is not a valid location", e.getMessage());
        }

        try {
            new GamePoint(6, 0);
            fail();
        } catch (InvalidLocationException e) {
            assertEquals("(6, 0) is not a valid location", e.getMessage());
        }

        try {
            new GamePoint(-1, 0);
            fail();
        } catch (InvalidLocationException e) {
            assertEquals("(-1, 0) is not a valid location", e.getMessage());
        }
    }

    @Test
    public void testInvalidYCoordinate() {
        try {
            new GamePoint(3, 0);
            fail();
        } catch (InvalidLocationException e) {
            assertEquals("(3, 0) is not a valid location", e.getMessage());
        }

        try {
            new GamePoint(27, 0);
            fail();
        } catch (InvalidLocationException e) {
            assertEquals("(27, 0) is not a valid location", e.getMessage());
        }

        try {
            new GamePoint(-1, 0);
            fail();
        } catch (InvalidLocationException e) {
            assertEquals("(-1, 0) is not a valid location", e.getMessage());
        }
    }

    @Test
    public void testInvalidCoordinates() {
        try {
            new GamePoint(3, 3);
            fail();
        } catch (InvalidLocationException e) {
            assertEquals("(3, 3) is not a valid location", e.getMessage());
        }

        try {
            new GamePoint(-12, 51);
            fail();
        } catch (InvalidLocationException e) {
            assertEquals("(-12, 51) is not a valid location", e.getMessage());
        }

        try {
            new GamePoint(15, -3);
            fail();
        } catch (InvalidLocationException e) {
            assertEquals("(15, -3) is not a valid location", e.getMessage());
        }
    }
}
