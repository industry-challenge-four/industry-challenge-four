package noughtsandcrosses.model.game;

import static org.junit.jupiter.api.Assertions.*;

import noughtsandcrosses.model.CellState;
import noughtsandcrosses.model.GamePoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import noughtsandcrosses.view.game.TestViewObserver;

import java.util.*;

public class TestBoardModel extends TestViewObserver {

    BoardModel sut;
    Deque<BoardEvent> eventReturned;

    /**
     * Because much of the noughtsandcrosses.model behaviour is publishing events, this defines a test observer that tracks all events heard in a deque so we can access them when needed
     */
    @BeforeEach
    public void setUp() {
        sut = new BoardModel();
        sut.setPlayerOne("Player One");
        sut.setPlayerTwo("Player Two");

        eventReturned = new ArrayDeque<>();

        ModelObserver testObserver = new ModelObserver() {
            @Override
            public void modelStateChange(BoardEvent event) {
                eventReturned.addLast(event);
            }

            @Override
            public void modelError(String message) {

            }
        };
        sut.addListener(testObserver);
    }

    @Test void testBoardModelSetHumanPlayers() {
        BoardModel sut = new BoardModel();
        sut.setPlayerOne("Player One");
        sut.setPlayerTwo("Player Two");

        assertEquals("Player One", sut.getPlayerOne());
        assertEquals("Player Two", sut.getPlayerTwo());
    }

    @Test void testBoardModelSetComputerPlayer() {
        BoardModel sut = new BoardModel();
        sut.setPlayerOne("Player One");
        sut.setPlayerTwo(BoardModel.COMPUTER);

        assertEquals("Player One", sut.getPlayerOne());
        assertEquals(BoardModel.COMPUTER, sut.getPlayerTwo());
    }

    @Test
    public void testComputerMoveFollowsPlayerMove() {
        BoardModel sut = new BoardModel();
        sut.setPlayerOne("Player One");
        sut.setPlayerTwo(BoardModel.COMPUTER);

        ModelObserver testObserver = new ModelObserver() {
            @Override
            public void modelStateChange(BoardEvent event) {
                eventReturned.addLast(event);
            }

            @Override
            public void modelError(String message) {

            }
        };
        sut.addListener(testObserver);


        Move move = new Move("Player One", new GamePoint(0, 0));
        sut.moveMade(move);

        BoardEvent playerMoveEvent = eventReturned.removeFirst();
        Map<GamePoint, CellState> stateAfterPlayerMove = playerMoveEvent.getState();

        BoardEvent computerMoveEvent = eventReturned.removeFirst();
        Map<GamePoint, CellState> stateAfterComputerMove = computerMoveEvent.getState();

        assertEquals(9, stateAfterPlayerMove.size());
        assertEquals(move, playerMoveEvent.getLastMove());
        assertNull(playerMoveEvent.getMessage());

        assertEquals(9, stateAfterComputerMove.size());
        assertEquals(new Move(BoardModel.COMPUTER, new GamePoint(1, 1)), computerMoveEvent.getLastMove());
        assertNull(computerMoveEvent.getMessage());

        stateAfterPlayerMove.forEach((gamePoint, cellState) -> {
            if (gamePoint.equals(move.getLocation())) {
                assertEquals(CellState.X, cellState);

            } else {
                assertEquals(CellState.EMPTY, cellState);
            }
        });

        stateAfterComputerMove.forEach((gamePoint, cellState) -> {
            if (gamePoint.equals(move.getLocation())) {
                assertEquals(CellState.X, cellState);

            } else if (gamePoint.equals(new GamePoint(1, 1))) {
                assertEquals(CellState.O, cellState);

            } else {
                assertEquals(CellState.EMPTY, cellState);
            }
        });
    }

    /**
     * The first player will always claim spaces using CellState.X
     */
    @Test
    public void testPlayerOneMoveMade() {
        Move move = new Move("Player One", new GamePoint(0, 0));
        sut.moveMade(move);

        BoardEvent event = eventReturned.removeFirst();
        Map<GamePoint, CellState> state = event.getState();

        assertEquals(9, state.size());
        assertEquals(move, event.getLastMove());
        assertNull(event.getMessage());

        state.forEach((gamePoint, cellState) -> {
            if (gamePoint.equals(move.getLocation())) {
                assertEquals(CellState.X, cellState);

            } else {
                assertEquals(CellState.EMPTY, cellState);
            }
        });

    }

    /**
     * The second player will always claim spaces using CellState.O
     */
    @Test
    public void testPlayerTwoMoveMade() {
        Move moveOne = new Move("Player One", new GamePoint(0, 0));
        sut.moveMade(moveOne);

        BoardEvent firstEventFired = eventReturned.removeFirst();

        Move moveTwo = new Move("Player Two", new GamePoint(1, 0));
        sut.moveMade(moveTwo);

        BoardEvent event = eventReturned.removeFirst();
        Map<GamePoint, CellState> state = event.getState();

        assertEquals(moveOne, firstEventFired.getLastMove());
        assertNull(firstEventFired.getMessage());

        assertEquals(9, state.size());
        assertEquals(moveTwo, event.getLastMove());
        assertNull(event.getMessage());

        state.forEach((gamePoint, cellState) -> {
            if (gamePoint.equals(moveOne.getLocation())) {
                assertEquals(CellState.X, cellState);

            } else if (gamePoint.equals(moveTwo.getLocation())) {
                assertEquals(CellState.O, cellState);

            } else {
                assertEquals(CellState.EMPTY, cellState);
            }
        });
    }

    @Test
    public void testGameReportsWinStateForPlayerOne() {
        List<BoardEvent> firedEvents = new ArrayList<>();

        Move moveOne = new Move("Player One", new GamePoint(1, 1));
        sut.moveMade(moveOne);

        firedEvents.add(eventReturned.removeFirst());

        Move moveTwo = new Move("Player Two", new GamePoint(0, 0));
        sut.moveMade(moveTwo);

        firedEvents.add(eventReturned.removeFirst());

        Move moveThree = new Move("Player One", new GamePoint(1, 2));
        sut.moveMade(moveThree);

        firedEvents.add(eventReturned.removeFirst());

        Move moveFour = new Move("Player Two", new GamePoint(0, 2));
        sut.moveMade(moveFour);

        firedEvents.add(eventReturned.removeFirst());

        Move moveFive = new Move("Player One", new GamePoint(1, 0));
        sut.moveMade(moveFive);

        BoardEvent event = eventReturned.removeFirst();

        assertEquals(4, firedEvents.size());
        assertEquals(moveFive, event.getLastMove());
        assertEquals("Player One wins!", event.getMessage());
    }

    @Test
    public void testGameReportsWinStateForPlayerTwo() {
        List<BoardEvent> firedEvents = new ArrayList<>();

        Move moveOne = new Move("Player One", new GamePoint(1, 1));
        sut.moveMade(moveOne);

        firedEvents.add(eventReturned.removeFirst());

        Move moveTwo = new Move("Player Two", new GamePoint(0, 0));
        sut.moveMade(moveTwo);

        firedEvents.add(eventReturned.removeFirst());

        Move moveThree = new Move("Player One", new GamePoint(1, 2));
        sut.moveMade(moveThree);

        firedEvents.add(eventReturned.removeFirst());

        Move moveFour = new Move("Player Two", new GamePoint(0, 2));
        sut.moveMade(moveFour);

        firedEvents.add(eventReturned.removeFirst());

        Move moveFive = new Move("Player One", new GamePoint(2, 0));
        sut.moveMade(moveFive);

        firedEvents.add(eventReturned.removeFirst());

        Move moveSix = new Move("Player Two", new GamePoint(0, 1));
        sut.moveMade(moveSix);

        BoardEvent event = eventReturned.removeFirst();

        assertEquals(5, firedEvents.size());
        assertEquals(moveSix, event.getLastMove());
        assertEquals("Player Two wins!", event.getMessage());
    }

    @Override
    public void testMoveMade() {
        Move move = new Move("Player One", new GamePoint(1, 1));
        sut.moveMade(move);

        BoardEvent event = eventReturned.removeFirst();

        assertEquals(9, event.getState().size());
        assertEquals(move, event.getLastMove());
        assertNull(event.getMessage());
    }

    @Override
    public void testHintRequested() {
        sut.hintRequested();

        BoardEvent event = eventReturned.removeFirst();

        assertEquals(9, event.getState().size());
        assertEquals("A good move for Player One might be left column, top row", event.getMessage());
        assertNull(event.getLastMove());
    }

    @Override
    public void testPlayAgain() {
        List<BoardEvent> firedEvents = new ArrayList<>();

        Move move = new Move("Player One", new GamePoint(0, 0));
        sut.moveMade(move);

        firedEvents.add(eventReturned.removeFirst());

        move = new Move("Player Two", new GamePoint(1, 1));
        sut.moveMade(move);

        firedEvents.add(eventReturned.removeFirst());

        move = new Move("Player One", new GamePoint(0, 1));
        sut.moveMade(move);

        firedEvents.add(eventReturned.removeFirst());

        move = new Move("Player Two", new GamePoint(1, 2));
        sut.moveMade(move);

        firedEvents.add(eventReturned.removeFirst());

        sut.playAgain();

        BoardEvent event = eventReturned.removeFirst();

        assertEquals(4, firedEvents.size());

        assertEquals(9, event.getState().size());
        assertEquals("New Game Started", event.getMessage());
        assertNull(event.getLastMove());

        event.getState().forEach((gamePoint, cellState) -> assertEquals(CellState.EMPTY, cellState));
    }



    // events with no BoardModel behaviour expected

    @Override
    public void testRecordsRequested() { }

    @Override
    public void testExit() { }
}
