package noughtsandcrosses;

import noughtsandcrosses.model.game.BoardModel;
import noughtsandcrosses.model.records.RecordController;
import noughtsandcrosses.view.game.BoardView;

import javax.swing.*;

public class Main {

    private void start() {
        BoardModel model = new BoardModel();
        BoardView view = new BoardView(model);
        RecordController rc = new RecordController();

        model.addListener(view);
        view.addListener(model);
        view.addListener(rc);

        SwingUtilities.invokeLater(view::createAndShowGUI);
    }

    public static void main(String[] args) {
        new Main().start();
    }

}
