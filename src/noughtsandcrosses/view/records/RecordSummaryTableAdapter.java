package noughtsandcrosses.view.records;

import noughtsandcrosses.model.records.RecordSummary;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class RecordSummaryTableAdapter extends AbstractTableModel {
    private final static String[] COLUMN_NAMES = {"Player", "Matches", "Wins", "Losses", "Draws"};
    private final List<RecordSummary> records;

    public RecordSummaryTableAdapter(List<RecordSummary> records) {
        this.records = records;
    }

    @Override
    public int getRowCount() {
        return records.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return records.get(row).getName();
            case 1:
                return records.get(row).getMatches();
            case 2:
                return records.get(row).getWins();
            case 3:
                return records.get(row).getLosses();
            default:
                return records.get(row).getDraws();
        }
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }
}
