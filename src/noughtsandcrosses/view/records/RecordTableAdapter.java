package noughtsandcrosses.view.records;

import noughtsandcrosses.model.records.Record;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class RecordTableAdapter extends AbstractTableModel {
    private final static String[] COLUMN_NAMES = {"Player One", "Player Two", "Winner", "Date Played"};
    private final List<Record> records;

    public RecordTableAdapter(List<Record> records) {
        this.records = records;
    }

    @Override
    public int getRowCount() {
        return records.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return records.get(row).getPlayerOne();
            case 1:
                return records.get(row).getPlayerTwo();
            case 2:
                return records.get(row).getResult();
            default:
                return records.get(row).getDatePlayed();
        }
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }
}
