package noughtsandcrosses.view.game;

import noughtsandcrosses.model.game.Move;

public interface ViewObserver {
    void moveMade(Move move);

    void hintRequested();

    void playAgain();

    void recordsRequested();

    void setNames(String playerOne, String playerTwo);

}
