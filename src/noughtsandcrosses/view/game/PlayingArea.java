package noughtsandcrosses.view.game;

import noughtsandcrosses.model.GamePoint;
import noughtsandcrosses.model.game.BoardModel;
import noughtsandcrosses.model.game.Move;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class PlayingArea extends JPanel {
    private final BoardModel model;

    private static final int PREFERRED_WIDTH = 500;

    private static final int PREFERRED_HEIGHT = 500;
    private static final int BORDER_AREA = 50;

    private static final int CELL_INTERNAL_BORDER = 20;
    public static final int BORDER_OFFSET = BORDER_AREA + CELL_INTERNAL_BORDER;

    private final List<ViewObserver> listeners;

    public PlayingArea(BoardModel model) {
        super();
        this.model = model;
        this.listeners = new ArrayList<>();

        setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                GamePoint location = gamePointFromPixelLocation(e.getPoint());
                if (location != null) {
                    listeners.forEach(listener -> listener.moveMade(new Move(location)));
                }
            }
        });
    }

    private GamePoint gamePointFromPixelLocation(Point pointClicked) {
        if (pointClicked.x < BORDER_AREA || pointClicked.x > (getWidth() - BORDER_AREA) || pointClicked.y < BORDER_AREA || pointClicked.y > (getHeight() - BORDER_AREA)) {
            return null;
        }

        int x = (pointClicked.x - BORDER_AREA) / getCellWidth();
        int y = (pointClicked.y - BORDER_AREA) / getCellHeight();

        return new GamePoint(x, y);
    }

    public void addListener(ViewObserver listener) {
        listeners.add(listener);
    }

    public void removeListener(ViewObserver listener) {
        listeners.remove(listener);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        int height = getHeight();
        int width = getWidth();
        g.drawLine(BORDER_AREA, BORDER_AREA, BORDER_AREA, height - BORDER_AREA);
        g.drawLine(BORDER_AREA, BORDER_AREA, width - BORDER_AREA, BORDER_AREA);

        g.drawLine(BORDER_AREA, height - BORDER_AREA, width - BORDER_AREA, height - BORDER_AREA);
        g.drawLine(width - BORDER_AREA, BORDER_AREA, width - BORDER_AREA, height - BORDER_AREA);

        int cellWidth = getCellWidth();
        g.drawLine(BORDER_AREA + cellWidth, BORDER_AREA, BORDER_AREA + cellWidth, height - BORDER_AREA);
        g.drawLine(BORDER_AREA + cellWidth * 2, BORDER_AREA, BORDER_AREA + cellWidth * 2, height - BORDER_AREA);

        int cellHeight = getCellHeight();
        g.drawLine(BORDER_AREA, BORDER_AREA + cellHeight, width - BORDER_AREA, BORDER_AREA + cellHeight);
        g.drawLine(BORDER_AREA, BORDER_AREA + cellHeight * 2, width - BORDER_AREA, BORDER_AREA + cellHeight * 2);

        renderBoard(g);

    }

    private void renderBoard(Graphics g) {
        model.getState().forEach((gamePoint, cellstate) -> {
            switch (cellstate) {
                case X:
                    drawX((Graphics2D)g.create(), gamePoint);
                    break;
                case O:
                    drawO((Graphics2D)g.create(), gamePoint);
                    break;
                case HINT:
                    drawHint((Graphics2D)g.create(), gamePoint);
            }
        });
    }

    private void drawO(Graphics2D g, GamePoint location) {
        int cellWidth = getCellWidth();
        int cellHeight = getCellHeight();
        int x = BORDER_OFFSET + ((cellWidth) * location.x);
        int y = BORDER_OFFSET + ((cellHeight) * location.y);

        g.setStroke(new BasicStroke(10));
        g.drawOval(x, y, cellWidth - CELL_INTERNAL_BORDER * 2, cellHeight - CELL_INTERNAL_BORDER * 2);
    }

    private void drawX(Graphics2D g, GamePoint location) {
        int cellWidth = getCellWidth();
        int cellHeight = getCellHeight();

        int internalWidth = cellWidth - CELL_INTERNAL_BORDER * 2;
        int internalHeight = cellHeight - CELL_INTERNAL_BORDER * 2;

        int leftX = BORDER_OFFSET + (cellWidth * location.x);
        int rightX = BORDER_OFFSET + (cellWidth * location.x) + internalWidth;

        int topY = BORDER_OFFSET + (cellHeight * location.y);
        int bottomY = BORDER_OFFSET + (cellHeight * location.y) + internalHeight;

        g.setStroke(new BasicStroke(10));

        g.drawLine(leftX, topY, rightX, bottomY);
        g.drawLine(rightX, topY, leftX, bottomY);
    }

    private void drawHint(Graphics2D g, GamePoint location) {
        int cellWidth = getCellWidth();
        int cellHeight = getCellHeight();

        int internalHeight = cellHeight - CELL_INTERNAL_BORDER * 2;
        int internalWidth = cellWidth - CELL_INTERNAL_BORDER;

        int x = BORDER_OFFSET + (cellWidth * location.x);
        int y = BORDER_OFFSET + (cellHeight * location.y) + internalHeight;

        g.setColor(Color.RED);
        g.setFont(new Font("Century Gothic", Font.PLAIN, cellHeight));
        g.drawString("?", x, y);
    }

    private int getCellWidth() {
        return (getWidth() - BORDER_AREA * 2) / 3;
    }

    private int getCellHeight() {
        return (getHeight() - BORDER_AREA * 2) / 3;
    }

}
