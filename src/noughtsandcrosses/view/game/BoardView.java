package noughtsandcrosses.view.game;

import noughtsandcrosses.model.game.BoardEvent;
import noughtsandcrosses.model.game.BoardModel;
import noughtsandcrosses.model.game.ModelObserver;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class BoardView extends JFrame implements ModelObserver {

    private final BoardModel model;
    private JTextArea playLog;

    private final List<ViewObserver> listeners;
    private JButton restartButton;
    private JButton changePlayers;

    private JLabel playerOneName;
    private JLabel playerTwoName;

    public BoardView(BoardModel model) throws HeadlessException {
        super();
        this.listeners = new ArrayList<>();
        this.model = model;
    }

    public void addListener(ViewObserver listener) {
        listeners.add(listener);
    }

    public void removeListener(ViewObserver listener) {
        listeners.remove(listener);
    }

    public void createAndShowGUI() {
        setTitle("Noughts and Crosses");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        configureContainer();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(screenSize.width / 2 - getWidth() / 2, screenSize.height / 2 - getHeight() / 2);

        requestPlayerNames();

        this.playLog.append(model.getPlayerOne() + " starts! \n");
        setVisible(true);
    }

    /**
     * Creates the dialog used to prompt the users to enter their names
     */
    private void requestPlayerNames() {
        PlayerNameDialog playerNameDialog = new PlayerNameDialog(model);
        listeners.forEach(playerNameDialog::addListener);
        playerNameDialog.requestPlayerNames();
    }

    /**
     * Updates the label containing the current player's names
     */
    private void updateNames() {
        playerOneName.setText("Crosses: " + model.getPlayerOne());
        playerTwoName.setText("Noughts: " + model.getPlayerTwo());
    }

    /**
     * configures the JFrame's content pane, initialising and configuring all the components
     */
    private void configureContainer() {
        Container visibleArea = getContentPane();
        visibleArea.setLayout(new BoxLayout(visibleArea, BoxLayout.PAGE_AXIS));

        JPanel names = createNamesPanel();
        visibleArea.add(names);

        PlayingArea playingArea = new PlayingArea(model);
        playingArea.addListener(model);
        visibleArea.add(playingArea);

        playLog = new JTextArea(8,50);
        JScrollPane scrollPane = new JScrollPane(playLog);
        visibleArea.add(scrollPane);

        JPanel buttonArea = createButtonArea();
        visibleArea.add(buttonArea);

        pack();
        playingArea.requestFocusInWindow();
    }

    /**
     * Creates and configures the panel that shows the current players names
     * @return configured playerNames JPanel
     */
    private JPanel createNamesPanel() {
        JPanel names = new JPanel();
        names.setLayout(new BorderLayout());
        names.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        playerOneName = new JLabel();
        playerTwoName = new JLabel();

        names.add(playerOneName, BorderLayout.LINE_START);
        names.add(playerTwoName, BorderLayout.LINE_END);

        return names;
    }

    /**
     * Creates the JPanel where the control buttons are, as well as the buttons themselves
     * @return configured ButtonArea JPanel
     */
    private JPanel createButtonArea() {
        JButton getHint = new JButton("Hint");
        getHint.addActionListener(event -> listeners.forEach(ViewObserver::hintRequested));

        configureRestartButton();

        JButton getRecords = new JButton("Records");
        getRecords.addActionListener(event -> listeners.forEach(ViewObserver::recordsRequested));

        configureChangeNamesButtons();

        JPanel buttonArea = new JPanel();
        buttonArea.setLayout(new FlowLayout());

        buttonArea.add(getHint);
        buttonArea.add(getRecords);
        buttonArea.add(restartButton);
        buttonArea.add(changePlayers);
        return buttonArea;
    }


    /**
     * Initialises and attaches the appropriate event handler to the change players button
     */
    private void configureChangeNamesButtons() {
        changePlayers = new JButton("Change Players");
        changePlayers.setEnabled(false);
        changePlayers.addActionListener(actionEvent -> {
            requestPlayerNames();
            restartButton.doClick();
        });
    }

    /**
     * Initialises and attaches the appropriate event handler to the restart button
     */
    private void configureRestartButton() {
        restartButton = new JButton("Play Again");
        restartButton.setEnabled(false);
        restartButton.addActionListener(actionEvent -> {
            listeners.forEach(ViewObserver::playAgain);
            restartButton.setEnabled(false);
            changePlayers.setEnabled(false);
        });
    }


    @Override
    public void modelStateChange(BoardEvent event) {
        updateNames();
        repaint();

        if (model.isComplete()) {
            restartButton.setEnabled(true);
            changePlayers.setEnabled(true);
        }

        if (event.getLastMove() != null) {
            playLog.append(event.getLastMove().toString() + "\n");
        }

        if (event.getMessage() != null) {
            playLog.append(event.getMessage() + "\n");
        }
    }

    @Override
    public void modelError(String message) {
        JOptionPane.showMessageDialog(null, message, "An error occurred", JOptionPane.ERROR_MESSAGE);
    }
}
