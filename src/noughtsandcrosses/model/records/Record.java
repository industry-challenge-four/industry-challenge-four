package noughtsandcrosses.model.records;


import java.time.LocalDate;

public class Record {

    private final String playerOne;
    private final String playerTwo;
    private final String result;
    private final LocalDate datePlayed;

    public Record(String playerOne, String playerTwo, String result, LocalDate datePlayed) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.result = result;
        this.datePlayed = datePlayed;
    }

    public String getPlayerOne() {
        return playerOne;
    }

    public String getPlayerTwo() {
        return playerTwo;
    }

    public String getResult() {
        return result;
    }
    public LocalDate getDatePlayed() {
        return datePlayed;
    }

}
