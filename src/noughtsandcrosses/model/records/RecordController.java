package noughtsandcrosses.model.records;

import noughtsandcrosses.model.game.Move;
import noughtsandcrosses.view.game.ViewObserver;
import noughtsandcrosses.view.records.RecordsView;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class RecordController implements ViewObserver {

    public static Connection getRecordDBConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:sqlite:records.db");
    }

    private void displayRecords() {
        try (Connection conn = getRecordDBConnection()) {
            List<Record> records = RecordDAO.getAllRecords(conn);
            List<RecordSummary> recordSummaries = RecordDAO.getAllRecordSummaries(conn);

            RecordsView recordsView = new RecordsView(records, recordSummaries);
            SwingUtilities.invokeLater(recordsView::createAndShowGUI);

        } catch (SQLException e) {
            SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "Unable to access database", "An error occurred", JOptionPane.ERROR_MESSAGE));
            e.printStackTrace();
        }
    }

    @Override
    public void recordsRequested() {
        if (!RecordsView.exists) {
            displayRecords();
        }
    }

    @Override
    public void moveMade(Move move) {
        // ignored
    }

    @Override
    public void hintRequested() {
        // ignored
    }

    @Override
    public void playAgain() {
        // ignored
    }

    @Override
    public void setNames(String playerOne, String playerTwo) {
        // ignored
    }

}
