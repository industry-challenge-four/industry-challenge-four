package noughtsandcrosses.model.records;

import noughtsandcrosses.model.game.BoardModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RecordDAO {

    public static boolean addRecord(Connection conn, Record record) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO records (playerOne, playerTwo, result, datePlayed) VALUES (?, ?, ?, ?)")) {
            stmt.setString(1, record.getPlayerOne());
            stmt.setString(2, record.getPlayerTwo());
            stmt.setString(3, record.getResult());
            stmt.setDate(4, Date.valueOf(record.getDatePlayed()));

            return stmt.execute();
        }
    }

    public static List<Record> getAllRecords(Connection conn) throws SQLException {
        List<Record> result = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {

            try (ResultSet r = stmt.executeQuery("SELECT * FROM records")) {

                while (r.next()) {
                    final int P_ONE = r.findColumn("playerOne");
                    final int P_TWO = r.findColumn("playerTwo");
                    final int RESULT = r.findColumn("result");
                    final int DATE = r.findColumn("datePlayed");

                    result.add(new Record(r.getString(P_ONE), r.getString(P_TWO), r.getString(RESULT), r.getDate(DATE).toLocalDate()));
                }

            }

        }

        return result;
    }

    public static List<RecordSummary> getAllRecordSummaries(Connection conn) throws SQLException {
        List<String> players = getAllPlayers(conn);
        List<RecordSummary> result = new ArrayList<>();

        for (String player : players) {
            try (PreparedStatement stmt = conn.prepareStatement(
                    "SELECT wins, draws, losses FROM " +
                            "(SELECT COUNT(result) wins FROM records " +
                            "WHERE (playerOne = ? OR playerTwo = ?) AND result = ?)," +
                            "(SELECT COUNT(result) draws FROM records " +
                            "WHERE (playerOne = ? OR playerTwo = ?) AND result = ?)," +
                            "(SELECT COUNT(result) losses FROM " +
                                "records " +
                            "WHERE " +
                                "(playerOne = ? OR playerTwo = ?) AND " +
                                "result != ? AND result != ?);"))
            {
                stmt.setString(1, player);
                stmt.setString(2, player);
                stmt.setString(3, player);
                stmt.setString(4, player);
                stmt.setString(5, player);
                stmt.setString(6, BoardModel.DRAW);
                stmt.setString(7, player);
                stmt.setString(8, player);
                stmt.setString(9, player);
                stmt.setString(10, BoardModel.DRAW);

                try (ResultSet r = stmt.executeQuery()) {
                    final int WINS = r.findColumn("wins");
                    final int DRAWS = r.findColumn("draws");
                    final int LOSSES = r.findColumn("losses");

                    result.add(new RecordSummary(player, r.getInt(WINS), r.getInt(LOSSES), r.getInt(DRAWS)));
                }
            }
        }

        return result;
    }

    public static List<String> getAllPlayers(Connection conn) throws SQLException {
        List<String> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            try (ResultSet r = stmt.executeQuery("SELECT DISTINCT playerOne FROM records " +
                                                    "UNION SELECT DISTINCT playerTwo FROM records;"))
            {
                while (r.next()) {
                    result.add(r.getString(1));
                }
            }
        }
        return result;
    }
}
