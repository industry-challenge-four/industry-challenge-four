package noughtsandcrosses.model.records;

public class RecordSummary {
    private final String name;
    private final int matches;
    private final int wins;
    private final int losses;
    private final int draws;

    public RecordSummary(String name, int wins, int losses, int draws) {
        this.name = name;
        this.wins = wins;
        this.losses = losses;
        this.draws = draws;
        this.matches = wins + losses + draws;
    }

    public String getName() {
        return name;
    }

    public int getMatches() {
        return matches;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public int getDraws() {
        return draws;
    }
}
