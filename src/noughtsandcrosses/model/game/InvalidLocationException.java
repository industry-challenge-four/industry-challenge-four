package noughtsandcrosses.model.game;

/**
 * Thrown when a move is given an invalid location
 * Unchecked since the user should never be able to manual enter a location's coordinates
 */
public class InvalidLocationException extends RuntimeException {
    public InvalidLocationException() {
        super();
    }

    public InvalidLocationException(String message) {
        super(message);
    }

    public InvalidLocationException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidLocationException(Throwable cause) {
        super(cause);
    }
}
