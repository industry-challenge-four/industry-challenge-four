package noughtsandcrosses.model.game;

import noughtsandcrosses.model.CellState;
import noughtsandcrosses.model.GamePoint;
import noughtsandcrosses.model.records.Record;
import noughtsandcrosses.model.records.RecordController;
import noughtsandcrosses.model.records.RecordDAO;
import noughtsandcrosses.view.game.ViewObserver;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardModel implements ViewObserver {
    public final static String COMPUTER = "computer";
    public final static String PLAYER_ONE_SYMBOL = "X";
    public final static String PLAYER_TWO_SYMBOL = "O";
    public final static String HINT_SYMBOL = "H";
    public final static String DRAW = "draw";

    private String state;
    private String playerOne;
    private String playerTwo;
    private boolean playerOneMove = true;
    private boolean hasWinner = false;

    private final List<ModelObserver> listeners;

    public BoardModel() {
        playerOne = null;
        playerTwo = null;
        listeners = new ArrayList<>();
        state = "012345678";
    }

    public String getPlayerOne() {
        return playerOne;
    }

    public void setPlayerOne(String playerOne) {
        this.playerOne = playerOne;
    }

    public String getPlayerTwo() {
        return playerTwo;
    }

    public void setPlayerTwo(String playerTwo) {
        this.playerTwo = playerTwo;
    }

    private String getCurrentPlayer() {
        return playerOneMove ? playerOne : playerTwo;
    }

    private String getCurrentPlayerSymbol() {
        return playerOneMove ? PLAYER_ONE_SYMBOL : PLAYER_TWO_SYMBOL;
    }

    private String getCurrentOpponent() {
        return playerOneMove ? playerTwo : playerOne;
    }

    private String getCurrentOpponentSymbol() {
        return playerOneMove ? PLAYER_TWO_SYMBOL : PLAYER_ONE_SYMBOL;
    }

    public boolean isComplete() {
        if (hasWinner) { return true; }

        for (int i = 0; i < 9; i++) {
            CellState cellState = getCellState(i);
            if (cellState == CellState.EMPTY || cellState == CellState.HINT) {
                return false;
            }
        }
        return true;
    }

    public Map<GamePoint, CellState> getState() {
        return mapCurrentState();
    }

    /**
     * Saves a record of the game to allow it to persist
     */
    private void saveRecord() {
        String result = hasWinner ? getCurrentPlayer() : DRAW;
        Record save = new Record(getPlayerOne(), getPlayerTwo(), result, LocalDate.now());

        try (Connection conn = RecordController.getRecordDBConnection()) {
            RecordDAO.addRecord(conn, save);
        } catch (SQLException e) {
            fireModelError("An error occurred. The game was not saved");
            e.printStackTrace();
        }
    }

    /**
     * Swaps the order of the players
     */
    private void swapPlayers() {
        String oldPlayerOne = playerOne;
        playerOne = playerTwo;
        playerTwo = oldPlayerOne;
    }

    public void addListener(ModelObserver listener) {
        listeners.add(listener);
    }

    public void removeListener(ModelObserver listener) {
        listeners.remove(listener);
    }

    private void fireModelStateChange(BoardEvent event) {
        listeners.forEach(listener -> listener.modelStateChange(event));
    }

    private void fireModelError(String message) {
        listeners.forEach(listeners -> listeners.modelError(message));
    }

    /**
     * Checks to see if it's the computers turn, and moves if it is
     * A computer player is defined as a player whose name is equal to BoardModel.COMPUTER
     */
    private void tryComputerMove() {
        if (isComplete()) {
            return;
        }

        if (getCurrentPlayer().equals(COMPUTER)) {
            BoardStateAnalyser analyser = new BoardStateAnalyser(state);
            moveMade(new Move(getCurrentPlayer(), analyser.getOptimalPosition(getCurrentPlayerSymbol(), getCurrentOpponentSymbol())));
        }
    }

    /**
     * Checks whether a location is claimed by a player
     * @param location GamePoint to check
     * @return true if claimed
     */
    private boolean isClaimedLocation(GamePoint location) {
        int internalPosition = getInternalPosition(location);
        CellState currentState = getCellState(internalPosition);
        return currentState == CellState.X || currentState == CellState.O;
    }

    /**
     * @return a map of GamePoint keys and their current CellState values
     */
    private Map<GamePoint, CellState> mapCurrentState() {
        Map<GamePoint, CellState> result = new HashMap<>();

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                GamePoint nextPoint = new GamePoint(i, j);
                CellState nextState = getCellState(j * 3 + i);
                result.put(nextPoint, nextState);
            }
        }

        return result;
    }

    /**
     * Updates the internal state to reflect a move by a player
     *
     * @param location GamePoint identifying a location to update
     * @param symbol   symbol for the player who has made the move (or the hint symbol to display a hint)
     */
    private void updateLocation(GamePoint location, String symbol) {
        int internalPosition = getInternalPosition(location);
        state = state.substring(0, internalPosition) + symbol + state.substring(internalPosition + 1);
    }

    /**
     * Converts a GamePoint into the corresponding index within the string that represents state
     * @param location GamePoint representation of a board location
     * @return integer index of the relevant char in the state string
     */
    private int getInternalPosition(GamePoint location) {
        return location.y * 3 + location.x;
    }

    /**
     * Returns the current state of the position specified
     *
     * @param position gameboard position; 0-8, left to right, top to bottom
     * @return CellState enum representing current state
     */
    private CellState getCellState(int position) {
        switch (state.charAt(position)) {
            case 'X':
                return CellState.X;

            case 'O':
                return CellState.O;

            case 'H':
                return CellState.HINT;

            default:
                return CellState.EMPTY;
        }
    }

    @Override
    public void moveMade(Move move) {
        if (isClaimedLocation(move.getLocation()) || hasWinner) {
            return; // do nothing if claimed or a player has won
        }

        move.setPlayer(getCurrentPlayer()); // the view will rely on the move.toString() method for publishing to the log so this needs to be updated
        updateLocation(move.getLocation(), getCurrentPlayerSymbol());

        BoardStateAnalyser analyser = new BoardStateAnalyser(state);
        if (analyser.isWinner(getCurrentPlayerSymbol())) {
            hasWinner = true;
            fireModelStateChange(new BoardEvent(mapCurrentState(), move, getCurrentPlayer() + " wins!"));
            saveRecord();
            swapPlayers();
            return;
        }

        if (isComplete()) { // is complete includes has won, so need to check for a win first
            fireModelStateChange(new BoardEvent(mapCurrentState(), move, "It's a draw!"));
            saveRecord();
            swapPlayers();
            return;
        }

        playerOneMove = !playerOneMove;
        fireModelStateChange(new BoardEvent(mapCurrentState(), move));

        tryComputerMove();
    }

    @Override
    public void hintRequested() {
        if (isComplete()) {
            return;
        }

        BoardStateAnalyser analyser = new BoardStateAnalyser(state);
        GamePoint optimalPosition = analyser.getOptimalPosition(getCurrentPlayerSymbol(), getCurrentOpponentSymbol());

        updateLocation(optimalPosition, HINT_SYMBOL);

        BoardEvent event = new BoardEvent(mapCurrentState(), "A good move for " + getCurrentPlayer() + " might be to claim " + optimalPosition.toString());
        fireModelStateChange(event);

    }

    @Override
    public void playAgain() {
        hasWinner = false;
        state = "012345678";
        playerOneMove = true;

        fireModelStateChange(new BoardEvent(mapCurrentState(), "Starting again: " + playerOne + " moves first"));
        tryComputerMove();
    }

    @Override
    public void setNames(String playerOne, String playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        fireModelStateChange(new BoardEvent(mapCurrentState(), "Players are now " + playerOne + " and " + playerTwo));
        tryComputerMove();
    }

    @Override
    public void recordsRequested() {
        // ignored by this listener
    }
}
