package noughtsandcrosses.model.game;

import noughtsandcrosses.model.CellState;
import noughtsandcrosses.model.GamePoint;

import java.util.Map;

public class BoardEvent {

    private final Map<GamePoint, CellState> state;
    private final Move lastMove;
    private final String message;

    public BoardEvent(Map<GamePoint, CellState> state, Move lastMove) {
        this.state = state;
        this.lastMove = lastMove;
        this.message = null;
    }

    public BoardEvent(Map<GamePoint, CellState> state, Move lastMove, String message) {
        this.state = state;
        this.lastMove = lastMove;
        this.message = message;
    }

    public BoardEvent(Map<GamePoint, CellState> state, String message) {
        this.state = state;
        this.lastMove = null;
        this.message = message;
    }

    public Map<GamePoint, CellState> getState() {
        return state;
    }

    public Move getLastMove() {
        return lastMove;
    }

    public String getMessage() {
        return message;
    }
}
