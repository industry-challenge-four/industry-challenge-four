package noughtsandcrosses.model.game;

public interface ModelObserver {
    void modelStateChange(BoardEvent event);

    void modelError(String message);
}
