package noughtsandcrosses.model;

import noughtsandcrosses.model.game.InvalidLocationException;

import java.awt.Point;

/**
 * Extension of the Point class with a customised toString method to make it easier to print points out in the view
 */
public class GamePoint extends Point {
    public GamePoint(int x, int y) {
        super(x, y);
        if (x < 0 || x > 2 || y < 0 || y > 2) {
            throw new InvalidLocationException("(" + x + ", " + y + ") is not a valid location");
        }
    }

    @Override
    public String toString() {
        String result;
        switch (x) {
            case 0:
                result = "left column, ";
                break;
            case 1:
                result = "centre column, ";
                break;
            default:
                result = "right column, ";
        }

        switch (y) {
            case 0:
                result += "top row";
                break;
            case 1:
                result += "centre row";
                break;
            default:
                result += "bottom row";
        }

        return result;
    }
}
