DROP TABLE IF EXISTS records;

CREATE TABLE IF NOT EXISTS records (
    matchId INTEGER PRIMARY KEY AUTOINCREMENT,
    playerOne VARCHAR(32),
    playerTwo VARCHAR(32),
    result VARCHAR(32) NOT NULL,
    datePlayed DATE
);